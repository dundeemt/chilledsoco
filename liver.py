'''test livereload'''

from livereload import Server, shell
from chilledsoco import wsgi_app
server = Server(wsgi_app())

# run a shell command
server.watch('templates/*')
server.watch('*.js')
server.watch('*.css')
server.watch('src/*')

server.serve(port=8080, open_url_delay=True)
