'''chilledsoco weblication'''
import os
# import urllib

import cherrypy
# from jinja2 import Environment, FileSystemLoader

from chilled import get_spkr
from chilled import ML_Item
import chilled

_LOG = cherrypy.log


def abspath(fname):
    '''return an absolute path use this file as the base path'''
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), fname)


def naked(template):
    '''return the template, no processing'''
    return file(abspath(template))


class UI:
    @cherrypy.expose
    def index(self):
        return naked('src/index.html')


class ApiBrowse:
    '''implement Browse'''
    exposed = True

    @cherrypy.tools.json_out()
    def GET(self, start=0, max_items=100, ml_items=None, subcats=None,
            term=None, **kwargs):
        _LOG('browse.%s' % ml_items)
        if ml_items == '-':
            ml_items = "SQ:,A:ARTIST,A:ALBUMARTIST,A:ALBUM,A:GENRE,A:COMPOSER,"
            ml_items += "A:TRACKS,A:PLAYLISTS"
        ml_items = ml_items.split(',')
        _LOG('browse.%s' % ml_items)
        _LOG("start: %s" % start)
        max_items = int(max_items)
        _LOG("max_items: %s" % max_items)

        _LOG("subcats: %s" % subcats)
        if subcats == '-':
            subcats = None
        try:
            subcatz = subcats.split(',')
        except AttributeError:
            subcatz = None
        _LOG("subcatz: %s" % subcatz)

        if term == '-':
            term = ''
        _LOG("term: %s" % term)

        rsp = {'meta': {
                    'contents': 'list',
                    'success': False,
                    'number_returned': 0,
                    'total_matches': 0
                },
               'contents': []
               }
        sonos = chilled.fastest_coordinator()
        for this_item in ml_items:
            ml_item = ML_Item(this_item)
            if this_item.startswith('SQ:') and (term is not None):
                rslt = chilled.sq_browse(coordinator=sonos, search_term=term)
            else:
                rslt = sonos.browse(ml_item=ml_item, start=start,
                                    max_items=max_items,
                                    full_album_art_uri=True,
                                    search_term=term, subcategories=subcatz)

            _LOG("ml_item: %s  returned: %i" % (ml_item, rslt.number_returned))
            rsp['meta']['success'] |= True
            rsp['meta']['number_returned'] += rslt.number_returned
            rsp['meta']['total_matches'] += rslt.total_matches
            rsp['contents'] += list(chilled.results(rslt))

        return rsp


class ApiSearch:
    '''implement search'''
    exposed = True

    @cherrypy.tools.json_out()
    def GET(self, start=0, max_items=100, search_type=None, subcats=None,
            term=None, **kwargs):
        _LOG("search_type: %s" % search_type)
        start = int(start)
        _LOG("start: %s" % start)
        max_items = int(max_items)
        _LOG("max_items: %s" % max_items)
        full = max_items == 0
        _LOG("complete: %s" % full)
        _LOG("subcats: %s" % subcats)
        if subcats == '-':
            subcats = None
        try:
            subcatz = subcats.split(',')
        except AttributeError:
            subcatz = None
        _LOG("subcatz: %s" % subcatz)
        if term == '-':
            term = ''
        _LOG("term: %s" % term)
        sonos = chilled.fastest_coordinator()
        try:
            rslt = sonos.get_music_library_information(search_type,
                                                       start=start,
                                                       max_items=max_items,
                                                       full_album_art_uri=True,
                                                       search_term=term,
                                                       subcategories=subcatz,
                                                       complete_result=full)

            rsp = {'meta': {
                        'contents': 'list',
                        'success': True,
                        'number_returned': rslt.number_returned,
                        'total_matches': rslt.total_matches
                    },
                   'contents': list(chilled.results(rslt))
                   }
        except KeyError as ke:
            rsp = {'meta': {
                        'contents': False,
                        'success': False,
                        'number_returned': 0,
                        'total_matches': 0,
                        'err': 'KeyError',
                        'errmsg': 'search_type: "%s" is not valid' % ke.message
                    },
                   'contents': []
                   }
            print dir(ke)
            print ke
        return rsp


class ApiSpeaker:
    exposed = True

    @cherrypy.tools.json_out()
    def GET(self, verb=None, spkr_ip=None, val=None, **kwargs):
        _LOG("verb: %s, spkr_ip: %s" % (verb, spkr_ip))

        if verb in Api.CUSTOM + Api.CUSTOML:
            _LOG("CUSTOM")
            func = getattr(chilled, verb)
            return func(spkr_ip)

        if verb in ['sonos_playlists', ]:
            _LOG("autocomplete")
            func = getattr(chilled, verb)
            return func(val)

        if verb in Api.GET_PROPERTIES:
            _LOG("STANDARD")
            return {verb: getattr(get_spkr(spkr_ip), verb)}

        if verb in Api.GMTHD:
            try:
                return getattr(get_spkr(spkr_ip), verb)()
            except chilled.soco.SoCoException:
                return {verb: 'Failed - Method unavailable'}

        _LOG('blind: %s' % str(getattr(get_spkr(spkr_ip), verb)))
        return {verb: getattr(get_spkr(spkr_ip), verb)}

    @cherrypy.tools.json_in(force=False)
    @cherrypy.tools.json_out()
    def POST(self, verb=None, spkr_ip=None, val=None, **kwargs):
        _LOG("verb: %s, spkr_ip: %s" % (verb, spkr_ip))
        # body = cherrypy.request.json
        # print "body:", body
        if verb in Api.BRW:
            sonos = get_spkr(spkr_ip)
            not_val = not getattr(sonos, verb)
            setattr(sonos, verb, not_val)
            return {verb: not_val}
        if verb in Api.MEDIA_CONTROLS:
            sonos = get_spkr(spkr_ip)
            return getattr(get_spkr(spkr_ip), verb)()
        if verb in Api.NRW + Api.PLAY_MODE:
            sonos = get_spkr(spkr_ip)
            setattr(get_spkr(spkr_ip), verb, val)
            return {verb: val}
        if verb in Api.QUEUE:
            sonos = get_spkr(spkr_ip)
            rslt = {'success': False, 'queue': [], 'current_track_info': {}}
            if verb == 'clear_queue':
                rslt['success'] = getattr(sonos, verb)()
                rslt['queue'] = chilled.get_queue(spkr_ip)
                rslt['current_track_info'] = sonos.get_current_track_info()
                del rslt['current_track_info']['metadata']
            if verb in ['play_from_queue', 'remove_from_queue']:
                rslt['success'] = getattr(sonos, verb)(int(val)-1)
                print "success: ", rslt['success']
                rslt['queue'] = chilled.get_queue(spkr_ip)
                rslt['current_track_info'] = sonos.get_current_track_info()
                del rslt['current_track_info']['metadata']
            if verb in ['add_uri_to_queue', 'play_uri']:
                body = cherrypy.request.json
                print "body:", body
                # returns ndx of added item
                ndx = getattr(sonos, verb)(body['uri'])
                print "ndx:", ndx
                rslt['success'] = True
                rslt['queue'] = chilled.get_queue(spkr_ip)
                rslt['current_track_info'] = sonos.get_current_track_info()
                del rslt['current_track_info']['metadata']
            if verb == 'create_sonos_playlist_from_queue':
                _LOG('create_sonos_playlist_from_queue: ' + val)
                rslt = getattr(sonos, verb)(title=val)
                print rslt
                print rslt.to_dict()

            return rslt


class ApiSpeakers:
    exposed = True

    @cherrypy.tools.json_out()
    def GET(self, verb=None, spkr_ip=None, **kwargs):
        return chilled.speakers()


class Api:
    '''create the api structure'''
    TODO = ['browse_by_idstring', 'create_sonos_playlist',
            'create_sonos_playlist_from_queue', 'get_album_artists',
            'get_albums', 'get_albums_for_artist', 'get_artists',
            'get_composers', 'get_favorite_radio_shows',
            'get_favorite_radio_stations', 'get_genres',
            'get_item_album_art_uri', 'get_music_library_information',
            'get_playlists', 'get_sonos_playlists', 'get_tracks',
            'get_tracks_for_album', 'partymode', 'search_track', 'seek',
            'start_library_update']
    # optioned read/write
    ORO = ['album_artist_display_option', ]  # WMP, ITUNES, NONE
    # string read only
    SRO = ['player_name', 'uid', 'ip_address', ]
    # boolean - read only
    BRO = ['is_bridge', 'is_coordinator', 'is_playing_line_in',
           'is_playing_radio', 'is_playing_tv', 'is_visible', 'status_light',
           'library_updating', ]
    # boolean read/write
    BRW = ['cross_fade', 'loudness', 'mute']
    # numeric - read/write
    NRW = ['volume', 'bass', 'treble']  # volume 0-100, bass/treble -10-10
    # numeric - read only
    NRO = ['queue_size', ]
    GMTHD = ['get_current_transport_info', 'switch_to_line_in', 'switch_to_tv']
    # playmode, return True othereise an exception
    MEDIA_CONTROLS = ['next', 'pause', 'play', 'previous', 'stop', ]
    PLAY_MODE = ['play_mode', ]
    # membership
    MEMBERSHIP = ['all_groups', 'all_zones', 'group', 'join', 'unjoin',
                  'visible_zones', ]
    # queue ops
    QUEUE = ['clear_queue', 'remove_from_queue', 'play_from_queue',
             'add_uri_to_queue', 'play_uri',
             'create_sonos_playlist_from_queue']
    # custom operations that return a dict
    CUSTOM = ['get_speaker_info', 'speaker_info',
              'get_current_track_info', 'current_transport_state', 'speaker']
    # custom methods that return a list
    CUSTOML = ['get_queue', ]
    # Queueable items POST
    PQUEUE = ['add_to_queue', 'add_item_to_sonos_playlist']
    GET_PROPERTIES = BRO + BRW + NRW + NRO + SRO
    GET_PROPERTIES += ORO


def app_cfg():
    '''return the app config'''
    cfg = {}
    cfg['/favicon.ico'] = {
                          'tools.staticfile.on': True,
                          'tools.staticfile.filename': abspath('favicon.ico'),
                          'tools.sessions.on': True,
                          }
    cfg['/api'] = {
                  'request.dispatch': cherrypy.dispatch.MethodDispatcher()
                  }
    cfg['/src'] = {
                  'tools.staticdir.on': True,
                  'tools.staticdir.dir': abspath('src')
    }
    return cfg


def make_app():
    '''return the app structure to serve'''
    root = UI()
    root.api = Api()
    root.api.speaker = ApiSpeaker()
    root.api.speakers = ApiSpeakers()
    root.api.search = ApiSearch()
    root.api.browse = ApiBrowse()
    return root


def wsgi_app():
    '''return cp ready to be run under a wsgi server'''
    wsgi = cherrypy.tree.mount(make_app(), config=app_cfg())
    # Disable the autoreload which won't play well
    cherrypy.config.update({'engine.autoreload.on': False})
    # let's not start the CherryPy HTTP server
    cherrypy.server.unsubscribe()
    # use CherryPy's signal handling
    cherrypy.engine.signals.subscribe()
    # Prevent CherryPy logs to be propagated
    # to the Tornado logger
    cherrypy.log.error_log.propagate = False
    # Run the engine but don't block on it
    cherrypy.engine.start()
    return wsgi


def main():
    '''the main loop'''

    root = make_app()
    cherrypy.config.update({'server.socket_host': '0.0.0.0',
                            'server.socket_port': 8080,
                            })

    cherrypy.quickstart(root, '/', app_cfg())


if __name__ == '__main__':
    main()
