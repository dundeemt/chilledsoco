'''chilled soco library == chilled'''

import datetime
import urllib

from dhp.search import fuzzy_search
import requests
import soco
from soco.data_structures import DidlPlaylistContainer
from soco.data_structures import DidlResource
from soco.exceptions import SoCoUPnPException
from soco.xml import XML

SONI = {x.ip_address: x for x in soco.discover(include_invisible=True)}
FASTEST_COOR = False

ATTRIBS = ['uid', 'status_light', 'player_name', 'ip_address', 'is_visible',
           'is_coordinator']
ATTRIBS_COOR = ['mute', 'volume', 'bass', 'treble', 'loudness', 'cross_fade',
                'play_mode', 'queue_size', 'album_artist_display_option',
                'is_playing_tv', 'is_playing_radio', 'is_playing_line_in']


class DeviceDescription(object):
    '''represent device_description.xml as an object'''
    def __init__(self, ip_address):
        self._base_url = 'http://%s:1400' % ip_address
        url = '%s/xml/device_description.xml' % self._base_url
        buf = requests.get(url).text
        self._tree = XML.fromstring(buf.encode('UTF-8'))

    def __simple(self, name, element=None):
        '''perform a findtext and return the result'''
        if element is None:
            element = self._tree
        srch = './/{urn:schemas-upnp-org:device-1-0}%s' % name
        return element.findtext(srch)

    @property
    def model_name(self):
        '''return the modelName'''
        return self.__simple('modelName')

    @property
    def icon_list(self, full_urls=True):
        '''iterator over icons in iconList'''
        elements = ['id', 'mimetype', 'width', 'height', 'depth', 'url']
        srch = './/{urn:schemas-upnp-org:device-1-0}iconList'
        for xicon in self._tree.find(srch).getchildren():
            icon = {}
            for element in elements:
                icon[element] = self.__simple(element, element=xicon)
            if full_urls:
                icon['url'] = self._base_url + icon['url']
            yield icon

    @property
    def serial_num(self):
        '''return the serialNum'''
        return self.__simple('serialNum')

    @property
    def memory(self):
        '''return the memory'''
        return int(self.__simple('memory'))

    @property
    def flash(self):
        '''return the flash'''
        return int(self.__simple('flash'))

    @property
    def amp_on_time(self):
        '''return the ampOnTime'''
        return int(self.__simple('ampOnTime'))


def fastest_coordinator():
    '''return the fastest coordinator to use for searches'''
    global FASTEST_COOR
    if FASTEST_COOR:
        print "fastest: returned cached"
        return FASTEST_COOR
    fastest = datetime.timedelta(days=100)
    for ip_address, sonos in SONI.iteritems():
        if not sonos.is_coordinator:
            # print "NOT COORD:", ip_address
            continue
        start = datetime.datetime.now()
        _ = sonos.get_music_library_information(search_type='artists',
                                                full_album_art_uri=True,
                                                complete_result=True)
        speed = datetime.datetime.now() - start
        print ip_address, speed
        if speed < fastest:
            FASTEST_COOR = sonos
            fastest = speed
            print "*", ip_address, fastest
    return FASTEST_COOR


def sq_browse(coordinator, search_term=None):
    '''return a SearchResult over the sonos queue titles'''
    rslts = coordinator.browse(ml_item=ML_Item('SQ:'))
    search_term = search_term or ''
    for i in range(len(rslts)-1, -1, -1):
        if search_term.lower() in rslts[i].title.lower():
            continue
        del rslts[i]
        rslts._metadata['number_returned'] -= 1
        rslts._metadata['total_matches'] -= 1
    return rslts


class ML_Item(object):
    def __init__(self, item_id):
        # self.item_id = urllib.quote(item_id, safe='')
        self.item_id = item_id

    def __repr__(self):
        return '<ML_Item item_id: "%s">' % self.item_id


class UnknownTypeError(ValueError):
    '''our own error type'''
    pass


def cmp_rslts(srslt, drslt):
    '''compare our result vs the to_dict results of the returned object to see
    if we are missing anything interesting.'''
    sdrslt = srslt.to_dict()
    ignored = ['restricted', 'resources', 'desc', ]
    for k in sdrslt:
        if k in ignored:
            continue
        if k not in drslt:
            raise UnknownTypeError('%s not in our result' % k)


def results(search_result):
    '''iterator to build proper dicts for conversion to json'''
    # print type(search_result)
    if type(search_result) != soco.data_structures.SearchResult:
        raise UnknownTypeError('Expected SearchResult')
    rtypes = {
        soco.data_structures.DidlMusicArtist: 'Artist',
        soco.data_structures.DidlSameArtist: 'SameArtist',
        soco.data_structures.DidlMusicAlbum: 'Album',
        soco.data_structures.DidlContainer: 'Container',
        soco.data_structures.DidlMusicTrack: 'Track',
        soco.data_structures.DidlAlbumList: 'AlbumList',
        soco.data_structures.DidlPlaylistContainer: 'PlayList',
        soco.data_structures.DidlMusicGenre: 'Genre',
        soco.data_structures.DidlComposer: 'Composer',
        soco.data_structures.DidlItem: 'Item',
    }
    for sresult in search_result:
        result = {}
        # All have
        result['type'] = rtypes[type(sresult)]
        result['item_id'] = sresult.item_id
        result['parent_id'] = sresult.parent_id
        result['title'] = sresult.title
        # synthesized
        if '/' not in sresult.item_id:
            result['encoded_id'] = sresult.item_id
        else:
            result['encoded_id'] = urllib.quote(sresult.item_id, safe=':()%')
        # may have
        result['album_art_uri'] = getattr(sresult, 'album_art_uri', None)
        result['album'] = getattr(sresult, 'album', None)
        result['creator'] = getattr(sresult, 'creator', None)
        # result['artist'] = getattr(sresult, 'artist', None)
        # result['date'] = getattr(sresult, 'date', None)
        result['original_track_number'] = getattr(sresult,
                                                  'original_track_number',
                                                  None)
        result['uri'] = sresult.resources[0].uri

        cmp_rslts(sresult, result)

        yield result


def search(search_type, start=0, max_items=None, subcats=None, term=None):
    '''run a search'''
    sonos = fastest_coordinator()
    args = {'search_type': search_type,
            'complete_result': False,
            'full_album_art_uri': False}
    try:
        start = int(start)
        args['start'] = start
    except (TypeError, ValueError):
        args['complete_result'] = True
    try:
        max_items = int(max_items)
        args['max_items'] = max_items
    except (TypeError, ValueError):
        args['complete_result'] = True

    if subcats is not None:
        subcats = [x.strip() for x in subcats.split(',')]
        args['subcategories'] = subcats
    if term is not None:
        args['search_term'] = term

    print args
    # print sonos.SEARCH_TRANSLATION
    rslts = sonos.get_music_library_information(**args)
    rslts = [sr.title for sr in rslts]
    return rslts


def speaker(ip_address):
    '''build a data element representing the speaker state and info'''
    # print ip_address
    spkr = get_spkr(ip_address)

    if spkr.is_coordinator:
        # print "is_coordinator"
        the_attribs = ATTRIBS + ATTRIBS_COOR
    else:
        # print "not is_coordinator"
        the_attribs = ATTRIBS
    # print the_attribs
    data = {attrib: getattr(spkr, attrib) for attrib in the_attribs}
    data['is_playing_music'] = data.get('is_playing_tv', False)
    data['is_playing_music'] |= data.get('is_playing_line_in', False)
    data['is_playing_music'] |= data.get('is_playing_tv', False)
    data['is_playing_music'] = not data['is_playing_music']
    if spkr.is_coordinator:
        rslt = spkr.get_current_track_info()
        del rslt['metadata']
        data['current_track_info'] = rslt
        rslt = spkr.get_current_transport_info()
        rslt = rslt['current_transport_state']
        data['current_transport_state'] = build_transport_state(rslt)
        data['queue'] = get_queue(ip_address)
        data['shuffle'], data['loop'] = make_loop_shuffle(data['play_mode'])
    return data


def make_loop_shuffle(buf):
    ''' calculate the loop int from the play_mode string'''
    playmode = ['NORMAL', 'REPEAT_ALL', 'SHUFFLE_NOREPEAT', 'SHUFFLE']
    playmode_int = playmode.index(buf)
    shuffle, loop = divmod(playmode_int, 2)
    return shuffle == 1, loop == 1


def build_transport_state(buf):
    '''build some flags to make ui easier'''
    tstate = {'playing': False, 'paused': False, 'stopped': False, 'name': buf}
    if buf == 'PAUSED_PLAYBACK':
        tstate['paused'] = True
    elif buf == 'PLAYING':
        tstate['playing'] = True
    elif buf == 'STOPPED':
        tstate['stopped'] = True
    return tstate


def speakers():
    '''build a list of speaker data elements'''
    def simple(spkr):
        attrs = ['player_name', 'uid', 'ip_address']
        return {aname: getattr(spkr, aname) for aname in attrs}
    data = [simple(zone) for zone in SONI.values() if zone.is_coordinator]
    data.sort(key=lambda x: x['player_name'])
    return data


def current_transport_state(ip_address):
    '''return the current transport state. STOPPED, PLAYING, PAUSED_PLAYBACK'''
    sonos = get_spkr(ip_address)
    rslt = sonos.get_current_transport_info()
    rslt = rslt['current_transport_state']

    return {'current_transport_state': build_transport_state(rslt)}


def get_sonos_info_full():
    '''return a fully populated list of Soco objects, querying each'''
    rslt = []
    for sonos in soco.discover(include_invisible=True):
        sonos.get_speaker_info()
        if sonos.is_visible:
            sonos.get_current_track_info()
        rslt.append(sonos)
        rslt.sort(key=lambda x: x.player_name)
    return rslt


def get_speaker_info(ip_address):
    '''return the speaker infor the sonos spkr at ip_address'''
    sonos = get_spkr(ip_address)
    return sonos.get_speaker_info()


def get_spkr(ip_address):
    '''return the SoCo object for the indicated address'''
    # return soco.SoCo(ip_address)
    return SONI[ip_address]


def get_queue(ip_address):
    '''return the list of music tracks in the queue'''
    sonos = get_spkr(ip_address)
    qsize = sonos.queue_size
    my_q = []
    for track in sonos.get_queue(max_items=qsize, full_album_art_uri=True):
        tdict = track.to_dict()
        tdict['uri'] = tdict['resources'][0].uri
        del tdict['resources']      # it's non-jsonifiable object DidlResource
        my_q.append(tdict)
    return my_q


def sonos_playlists(term):
    '''return a list of Sonos playlists fuzzy filtered by term'''
    # good place to use dhp.fuzzy_search
    if term is None:
        term = ''
    term = term.lower()
    sonos = fastest_coordinator()
    haystack = [item.title for item in sonos.get_sonos_playlists()]
    rspns = fuzzy_search(needle=term, haystack=haystack)
    return rspns


def update_sonos_playlist_from_queue(sonos, title):
    """ Update/Create a Sonos playlist from the current queue based on title.
        :params title: Name of the playlist
        :returns: An instance of
            :py:class:`~.soco.data_structures.DidlPlaylistContainer`
    """
    # Note: probably same as Queue service method SaveAsSonosPlaylist
    # but this has not been tested.  This method is what the
    # controller uses.
    object_id = ''
    for plist in sonos.get_sonos_playlists():
        if plist.title == title:
            object_id = plist.item_id
            break
    print "object_id:", object_id
    response = sonos.avTransport.SaveQueue([
        ('InstanceID', 0),
        ('Title', title),
        ('ObjectID', object_id)
    ])
    item_id = response['AssignedObjectID']
    obj_id = item_id.split(':', 2)[1]
    uri = "file:///jffs/settings/savedqueues.rsq#{0}".format(obj_id)
    res = [DidlResource(uri=uri, protocol_info="x-rincon-playlist:*:*:*")]
    return DidlPlaylistContainer(
        resources=res, title=title, parent_id='SQ:', item_id=item_id)


def delete_sonos_playlist(sonos, title=None, object_id=None):
    """ delete a Sonos playlist by title OR object_id.  If both are specified,
        the function will fail. You should specify one or the other but NOT
        both.

        :params title: Name of the playlist
        :params object_id: the id of the sonos playlist to delete. i.e. 'SQ:1'
        :returns: boolean
    """
    title = title or ''
    object_id = object_id or ''
    if not title and not object_id:
        return False
    if title and object_id:
        return False
    if not object_id:
        for plist in sonos.get_sonos_playlists():
            if plist.title == title:
                object_id = plist.item_id
                break
    if not object_id:
        return False
    try:
        response = sonos.contentDirectory.DestroyObject([('ObjectID',
                                                          object_id)])
    except SoCoUPnPException as err:
        if err.error_code == '701':   # No Such Object
            response = False
        else:
            raise

    return response


def get_current_track_info(ip_address):
    '''return current track info'''
    sonos = get_spkr(ip_address)
    rslt = sonos.get_current_track_info()
    del rslt['metadata']
    return rslt


def speaker_info(ip_address):
    '''returned the cached information, if available'''
    sonos = get_spkr(ip_address)
    rslt = sonos.speaker_info
    if rslt:
        return rslt
    return sonos.get_speaker_info()


def to_dict(itm):
    '''convert the DidlContainer and it's children to a dict'''
    dit = itm.to_dict()
    dit = {k: v for k, v in dit.iteritems() if v is not None}
    if 'resources' in dit:
        dit['resources'] = [resource_to_dict(rez, optimized=True)
                            for rez in dit['resources']]
    return dit


def resource_to_dict(didlresource, optimized=False):
    '''transform a DidlResource to a dictionary'''
    content = {
        'uri': didlresource.uri,
        'protocol_info': didlresource.protocol_info,
        'import_uri': didlresource.import_uri,
        'size': didlresource.size,
        'duration': didlresource.duration,
        'bitrate': didlresource.bitrate,
        'sample_frequency': didlresource.sample_frequency,
        'bits_per_sample': didlresource.bits_per_sample,
        'nr_audio_channels': didlresource.nr_audio_channels,
        'resolution': didlresource.resolution,
        'color_depth': didlresource.color_depth,
        'protection': didlresource.protection,
    }
    if optimized:
        # strip any elements that have a value of None to optimize size
        # of the returned structure
        nones = [k for k in content if content[k] is None]
        for k in nones:
            del content[k]
    return content


def main():
    '''main loop'''
    print SONI
    sonos = fastest_coordinator()
    print "Fastest:", sonos
    print "Groups:"
    for group in sonos.all_groups:
        print group

    print dir(sonos)
    # print help(sonos.deviceProperties)
    print sonos.get_music_library_information('artists',
                                              start=0,
                                              max_items=20,
                                              full_album_art_uri=True,
                                              search_term='Al',
                                              subcategories=[],
                                              complete_result=False)
if __name__ == '__main__':
    main()
