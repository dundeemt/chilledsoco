'''test the api inteface'''
# the following 3 lines let py.test find the module
import sys
import os
MYPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MYPATH + '/../')
import json

import requests
from chilledsoco import Api

import pytest
sonos_speaker = pytest.mark.sonos_speaker
chilled_server = pytest.mark.cs_server


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.BRO)
def test_bro(cs_server, sonos_ip, verb):
    '''test the Boolean Read/Only verbs'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    assert type(rspns.json()[verb]) is bool


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.ORO)
def test_oro(cs_server, sonos_ip, verb):
    '''test the optioned read only verbs'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    jsn = rspns.json()[verb]
    assert type(jsn) is unicode
    assert jsn in ['WMP', 'ITUNES', 'NONE']


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.NRO)
def test_nro(cs_server, sonos_ip, verb):
    '''test the Numeric Read/Only verbs'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    assert type(rspns.json()[verb]) is int


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.SRO)
def test_sro(cs_server, sonos_ip, verb):
    '''test the String Read/Only verbs'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    assert type(rspns.json()[verb]) is unicode


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.BRW)
def test_brw(cs_server, sonos_ip, verb):
    '''test the boolean Read/Write verbs'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    val = rspns.json()[verb]
    assert type(val) is bool
    rspns = requests.post(url % (cs_server, verb, sonos_ip))
    # ,
    #                       data=json.dumps({"val": True}),
    #                       headers={'Content-Type': 'application/json'})
    nval = rspns.json()[verb]
    assert type(nval) is bool
    assert val != nval


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.NRW)
def test_nrw_read(cs_server, sonos_ip, verb):
    '''test the Numeric Read/Write verbs'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    assert type(rspns.json()[verb]) is int


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.NRW)
def test_nrw_write(verb, cs_server, sonos_ip):
    '''test writing to numeric read/write verbs'''
    gurl = 'http://%s/api/speaker/%s/%s/' % (cs_server, verb, sonos_ip)
    rspns = requests.get(gurl)
    oval = int(rspns.json()[verb])
    nval = oval + 1
    purl = 'http://%s/api/speaker/%s/%s/%s/'
    rspns = requests.post(purl % (cs_server, verb, sonos_ip, nval))
    wval = int(rspns.json()[verb])
    # restore original val
    rspns = requests.post(purl % (cs_server, verb, sonos_ip, oval))
    # test the outputs
    assert int(rspns.json()[verb]) == oval
    assert nval == wval


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.CUSTOM)
def test_custom(cs_server, sonos_ip, verb):
    '''test the custom verbs'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    assert type(rspns.json()) is dict


@chilled_server
@sonos_speaker
@pytest.mark.parametrize("verb", Api.CUSTOML)
def test_customl(cs_server, sonos_ip, verb):
    '''test the custom verbs that return lists'''
    url = 'http://%s/api/speaker/%s/%s/'
    rspns = requests.get(url % (cs_server, verb, sonos_ip))
    assert type(rspns.json()) is list


@chilled_server
def test_speakers(cs_server):
    '''test the speakers data endpoint'''
    url = 'http://%s/api/speakers/' % cs_server
    rspns = requests.get(url)
    assert type(rspns.json()) is list
