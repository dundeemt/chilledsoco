'''py.test config'''
# content of conftest.py
import pytest


def pytest_addoption(parser):
    '''add a cmd line option for the ip address of a sonos speaker'''
    parser.addoption("--sonos", action="store", default="",
                     help="ip address of sonos speaker")
    parser.addoption("--server", action="store", default="",
                     help="ipaddress:port of a chilledsoco server")


@pytest.fixture
def sonos_ip(request):
    '''make a fixture out of ip addr info'''
    return request.config.getoption("--sonos")


@pytest.fixture
def cs_server(request):
    '''make a fixture out of the chilledsoco server option'''
    return request.config.getoption("--server")


def pytest_runtest_setup(item):
    '''act on skip marker'''
    print list(item.keywords)
    if 'sonos_speaker' in item.keywords:
        if not item.config.getoption("--sonos"):
            pytest.skip("need --sonos to run this integration test")
    if 'cs_server' in item.keywords:
        if not item.config.getoption("--server"):
            pytest.skip("need --server to run this integration test")
