'''test the chilled library'''
# the following 3 lines let py.test find the module
import sys
import os
MYPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MYPATH + '/../')

import chilled

import pytest

sonos_speaker = pytest.mark.sonos_speaker

SKEYS = chilled.soco.SoCo.SEARCH_TRANSLATION.keys()


def test_search_empty():
    '''just a call to search, no values'''
    with pytest.raises(TypeError):
        _ = chilled.search()


@pytest.mark.parametrize("sterm", SKEYS)
def test_search_search_type(sterm):
    '''responds to all recognized search terms'''
    # (search_type=None, start=None, max_items=None, subcats=None, term=None)
    rslt = chilled.search(sterm, max_items=10)
    assert type(rslt) == list
    assert type(rslt[0]) == unicode


def test_search_bad_search_type():
    '''test errors out with bad search term'''
    with pytest.raises(KeyError):
        assert chilled.search('foo')


def test_search_start_no_max():
    '''test that start with no max results in complete_result'''
    rslt = chilled.search('genres', start=0)
    assert len(rslt) > len(chilled.search('genres', start=0, max_items=100))


def test_search_0_based():
    '''test that search is 0 based.'''
    base_0 = chilled.search('genres', start=0, max_items=10)
    base_1 = chilled.search('genres', start=1, max_items=10)
    assert base_0 != base_1


def test_search_no_start():
    '''test the not specifying a start value is ok, and gives expected'''
    start_specd = chilled.search('genres', start=0, max_items=10)
    no_start = chilled.search('genres', max_items=10)
    assert start_specd == no_start
    assert len(start_specd) == 10


def test_search_1_subcat():
    '''test search with one subcategory'''
    rslts = chilled.search('genres', max_items=10)
    # print rslts
    subcat = rslts[-1]
    rslts = chilled.search('genres', subcats=subcat)
    # print rslts
    assert type(rslts) == list
    assert (type(rslts[0])) == unicode


def test_search_2_subcat():
    '''test search with two subcategories'''
    rslts = chilled.search('genres', max_items=10)
    # print rslts
    subcat1 = rslts[-1]
    rslts = chilled.search('genres', subcats=subcat1)
    # print rslts
    subcat2 = rslts[-1]
    rslts = chilled.search('genres', subcats='%s,%s' % (subcat1, subcat2))
    # print rslts
    assert type(rslts) == list
    assert (type(rslts[0])) == unicode


@sonos_speaker
def test_delete_sonos_plst_bad_id(sonos_ip):
    '''delete object_id that doesn't exist'''
    sonos = chilled.get_spkr(sonos_ip)
    next_id = 0
    for plst in sonos.get_sonos_playlists():
        this_id = int(plst.item_id.split(':')[1])
        if this_id >= next_id:
            next_id = this_id + 1
    object_id = 'SQ:%i' % next_id
    print 'Deleting %s' % object_id
    assert chilled.delete_sonos_playlist(sonos, object_id=object_id) == False


@sonos_speaker
def test_delete_sonos_plst_both(sonos_ip):
    '''specify both title and object_id is a fail'''
    sonos = chilled.get_spkr(sonos_ip)
    next_id = 0
    for plst in sonos.get_sonos_playlists():
        this_id = int(plst.item_id.split(':')[1])
        if this_id >= next_id:
            next_id = this_id + 1
    object_id = 'SQ:%i' % next_id
    title = 'Foo'
    print 'Deleting: object_id %s title=%s' % (object_id, title)
    assert chilled.delete_sonos_playlist(sonos, title=title,
                                         object_id=object_id) == False


@sonos_speaker
def test_delete_sonos_plst_neither(sonos_ip):
    """don't specify either the title or object_id - should fail"""
    sonos = chilled.get_spkr(sonos_ip)
    assert chilled.delete_sonos_playlist(sonos) == False
