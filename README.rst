===========
chilledsoco
===========

A web ui over the soco library to control your Sonos system.


Goals
=====
* web ui based on material design and implemented with angularjs
* served via cherrypy so it can run just about anywhere


Development
===========
#. ``pip install -r dev-requirements.txt``
#. run the non-integration tests  ``py.test``
#. start a livereload version of the app  ``python liver.py``


liver.py
========
* expects browser to have a livereload plugin (install via chome store)
* will autostart the browser and/or open a new tab
* changes to files under src/ will trigger a live reload in that tab
* changes to the cherrypy app will trigger a complete reload into a new tab
* see docs/notes.txt for helpful resources


chilledsoco.py
==============
* the application, production mode
* ``python chilledsoco.py``