'use strict';
(function(){
    var app = angular.module('chilledSoco', ['ngMaterial', 'ngResource']);

    app.factory("api", function($resource) {
      return {
        speakers: $resource("/api/speakers/", {}).query,
        toggleBRW: $resource('/api/speaker/:verb/:ip_address',
                             {verb: '@verb', ip_address: '@ip_address'}).save,
        speaker: $resource('/api/speaker/:verb/:ip_address/:num',
                           {verb: '@verb',
                            ip_address: '@ip_address',
                            num: '@num'}),
        browse: $resource('/api/browse/:start/:maxrecs/:mlitem/:subcats/:term',
                          {
                            start:   '@start',
                            maxrecs: '@maxrecs',
                            mlitem:  '@mlitem',
                            subcats: '@subcats',
                            term:    '@term'
                          })
      }
    });

    app.service('speakerModel', ['api', function(api) {
        var vm = this;

        vm.updateSelectedSpeaker = function(){
            // method to update .selectedSpeaker from .speakers, if set, else
            // update to the first record in .speakers
            var ip_address = vm.speakers[0].ip_address;;
            if (vm.selectedSpeaker !== null){
                ip_address = vm.selectedSpeaker.ip_address
            };
            var params = {verb: 'speaker', ip_address: ip_address};
            vm.selectedSpeaker = api.speaker.get(params, function(){
                console.log('.updateSelectedSpeaker: '+ vm.selectedSpeaker.uid);
            });
        };      // end .updateSelectedSpeaker

        vm.updateSpeakers = function() {
            // method to refresh .speakers and .selectedSpeaker
            console.log('.updateSpeakers');
            vm.speakers = api.speakers(function(){
                vm.updateSelectedSpeaker();
            });
        };      // end .updateSpeakers

        vm.selectSpeaker = function(speaker) {
            console.log('.selectedSpeaker: '+speaker.uid);
            vm.selectedSpeaker = speaker;
            vm.updateSelectedSpeaker();
        };      // end selectSpeaker

        vm.toggleBRW = function(speaker, verb) {
            // toggle verbs
            var params = {verb: verb,
                          ip_address: speaker.ip_address};
            console.log('.toggleBRW: '+JSON.stringify(params));
            var result = api.toggleBRW(params);
            console.log('BRWtoggled: '+JSON.stringify(result));
        };      // end .toggleBRW

        vm.updateNRW = function(verb, ip_address, num) {
            // notify the backend of the change to a NRW value
            var params = {verb: verb, ip_address: ip_address, num: num};
            console.log('.updateNRW: '+JSON.stringify(params));
            var result = api.speaker.save(params);
            console.log('NRWupdated: ' +JSON.stringify(result));
        };

        vm.changedNRW = function(verb) {
            // receive a notification that a specifc NRW attr has changed
            var num = vm.selectedSpeaker[verb]
            var ip_address = vm.selectedSpeaker.ip_address;
            console.log('changedNRW: verb->'+ verb+'  num->'+num);
            vm.updateNRW(verb, ip_address, num);
        }

        vm.changedPM = function() {
            // update play_mode based on virtual soco attrs: lopp and shuffle
            var verb = 'play_mode';
            var modes = ['NORMAL', 'REPEAT_ALL', 'SHUFFLE_NOREPEAT', 'SHUFFLE'];
            var ndx = 0;
            var ip_address = vm.selectedSpeaker.ip_address;
            if (vm.selectedSpeaker.loop){ ndx = ndx + 1; };
            if (vm.selectedSpeaker.shuffle){ ndx = ndx + 2; };
            console.log('pmChange: Loop->'+vm.selectedSpeaker.loop +' shuffle->'+vm.selectedSpeaker.shuffle);
            vm.selectedSpeaker.play_mode = modes[ndx];
            console.log('PlayMode: '+ vm.selectedSpeaker.play_mode);
            vm.updateNRW(verb, ip_address, modes[ndx]);
        }

        vm.setMediaControl = function(speaker, verb) {
            // set the playmode
            var params = {verb: verb, ip_address: speaker.ip_address};
            console.log('setMediaControl: '+JSON.stringify(params));
            var result = api.speaker.save(params, function(){
                // vm.updateSpeakers();
                vm.updateSelectedSpeaker();
            });
            console.log('setMediaControl result: '+JSON.stringify(result));
        };      // end setMediaControl

        function wasClick(element, index, array) {
            return (element.term !== '');
         };

        vm.termChange = function() {
            console.log('termChange: '+vm.browseTerm);
            var params = {
                start: 0,
                maxrecs: 20,
                mlitem: '-',
                subcats: '-',
                term: vm.browseTerm
            };
            if (vm.lastParams){vm.browsePath.push(vm.lastParams);}
            console.log('.browsePath: '+JSON.stringify(vm.browsePath));
            vm.browsePath  = vm.browsePath.filter(wasClick);
            console.log('.browsePath: '+JSON.stringify(vm.browsePath));
            vm.lastParams = params;
            // console.log(JSON.stringify(params));
            vm.browseResults = api.browse.get(params, function(){
                // console.log(JSON.stringify(vm.browseResults));
            });
        };      // end termChange

        vm.termClear = function() {
            console.log('.termClear');
            vm.browseTerm = '';
            vm.browseResults = null;
            vm.browsePath = [];
        };

        vm.browseBack = function(){
            // move back on the browsePath stack if possible
            console.log('.browsePath: '+JSON.stringify(vm.browsePath));
            var params = vm.browsePath.pop();
            console.log('.browsePath: '+JSON.stringify(vm.browsePath));
            console.log('.browseBack ->' + JSON.stringify(params));
            vm.browseTerm = params.term;
            vm.browseResults = api.browse.get(params, function(){
                // console.log(JSON.stringify(vm.browseResults));
            });
        };

        vm.resultClick = function(rslt) {
            console.log('resultClick: '+rslt.encoded_id+' type->'+rslt.type);
            if (rslt.type === 'Track'){ return };
            var params = {
                start: 0,
                maxrecs: 20,
                mlitem: rslt.encoded_id,
                subcats: '-',
                term: ''
            };
            console.log('.browsePath: '+JSON.stringify(vm.browsePath));
            if (vm.lastParams){vm.browsePath.push(vm.lastParams);}
            console.log('.browsePath: '+JSON.stringify(vm.browsePath));
            vm.lastParams = params;
            console.log(JSON.stringify(params));
            vm.browseResults = api.browse.get(params, function(){
                // console.log(JSON.stringify(vm.browseResults));
            });
        };      // end resultClick


        vm.playNow = function(rslt) {
            // requested to play the thing now
            console.log('.playNow: '+rslt.encoded_id+' type->'+rslt.type);
            var ip_address = vm.selectedSpeaker.ip_address;
            var params = {verb: 'play_uri', ip_address: ip_address};
            var body = {uri: rslt.uri}
            console.log('.playNow: '+ JSON.stringify(body));
            var result = api.speaker.save(params, body, function(rspns){
                console.log('playNow -> '+JSON.stringify(rspns));
                vm.selectedSpeaker.queue = rspns.queue;
                vm.selectedSpeaker.current_track_info = rspns.current_track_info;
            });
        };

        vm.queueSaveAs = function(name) {
            console.log('.queueSaveAs ->'+name);
            var params = {verb: 'create_sonos_playlist_from_queue',
                          ip_address: vm.selectedSpeaker.ip_address,
                          num: name
                          };
            console.log('params ->'+JSON.stringify(params));
            api.speaker.save(params, function(rspns){
                console.log('rspns->'+JSON.stringify(rspns));
            });
        };

        vm.queueAdd = function(rslt) {
            // requested to queue the thing
            console.log('.queueAdd: '+rslt.encoded_id+' type->'+rslt.type);
            var ip_address = vm.selectedSpeaker.ip_address;
            var params = {verb: 'add_uri_to_queue', ip_address: ip_address};
            var body = {uri: rslt.uri}
            console.log('.queueAdd: '+ JSON.stringify(body));
            var result = api.speaker.save(params, body, function(rspns){
                console.log('queueAdd -> '+JSON.stringify(rspns));
                vm.selectedSpeaker.queue = rspns.queue;
                vm.selectedSpeaker.current_track_info = rspns.current_track_info;
            });
        };

        vm.queueClear = function() {
            // request to clear queue
            console.log('speakerModel.queueClear');
            var ip_address = vm.selectedSpeaker.ip_address;
            var params = {verb: 'clear_queue', ip_address: ip_address};
            var result = api.speaker.save(params, function(rspns){
                console.log('queueClear -> '+JSON.stringify(rspns));
                vm.selectedSpeaker.queue = rspns.queue;
                vm.selectedSpeaker.current_track_info = rspns.current_track_info;
            });
        };

        vm.queuePlay = function(ndx) {
            // request to play song at queue with a index of ndx
            console.log('speakerModel.queuePlay('+ndx+')');
            var ip_address = vm.selectedSpeaker.ip_address;
            var verb = 'play_from_queue';
            var params = {verb: verb, ip_address: ip_address, num: ndx};
            var result = api.speaker.save(params, function(rspns){
                console.log('queuePlay -> '+JSON.stringify(rspns));
                vm.selectedSpeaker.queue = rspns.queue;
                vm.selectedSpeaker.current_track_info = rspns.current_track_info;
            });
        };

        vm.queueRemove = function(ndx){
            // request to remove song at ndx from queue
            console.log('speakerModel.queueRemove('+ndx+')');
            var ip_address = vm.selectedSpeaker.ip_address;
            var verb = 'remove_from_queue';
            var params = {verb: verb, ip_address: ip_address, num: ndx};
            var result = api.speaker.save(params, function(rspns){
                console.log('queueRemove -> '+JSON.stringify(rspns));
                vm.selectedSpeaker.queue = rspns.queue;
                vm.selectedSpeaker.current_track_info = rspns.current_track_info;
            });
        };

        vm.getSonosPlaylists = function(term) {
            // call the api and return a list of sonos queue titles
            console.log('.getSonosPlaylists');
            var params = {verb: 'sonos_playlists',
                          ip_address: '-',
                          num: term
                         };
            return api.speaker.query(params);
        };

        console.log('speakerModel::init');
        vm.selectedSpeaker = null;
        vm.speakers = [];
        vm.browseTerm = '';
        vm.browseResults = {};
        vm.browsePath = [];
        vm.lastParams = null;

        vm.updateSpeakers();
    }]);     // end of speakerModel service

    app.directive('csAngularEnabled', function() {
        return {
            restrict: "E",
            template: "angular enabled: {{ ae.datetime | date:'yyyy-MM-dd HH:mm:ss'}}",
            controller: function(){
                var vm = this;
                vm.datetime = new Date();
            },
            controllerAs: "ae"
        }
    });

    app.directive('csToolbar', ['$mdSidenav', function($mdSidenav) {
        return {
            restrict: "E",
            template: '<md-toolbar layout="row"> \
                         <div class="md-toolbar-tools"> \
                           <md-button ng-click="at.toggleSidenav(\'left\')" hide-gt-sm class="md-icon-button"> \
                             <md-icon aria-label="Menu" md-svg-icon="https://s3-us-west-2.amazonaws.com/s.cdpn.io/68133/menu.svg"> \
                             </md-icon> \
                           </md-button> \
                           <h1>Chilled SoCo -- aaah!</h1> \
                         </div> \
                      </md-toolbar>',
            controller: function(){
                var vm = this;

                vm.toggleSidenav = function(menuId) {
                    $mdSidenav(menuId).toggle();
                };

            },
            controllerAs: "at"
        }
    }]);

    app.directive('csSidenav', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            template: '\
            <md-sidenav layout="column" class="md-sidenav-left md-whiteframe-z2" md-component-id="left" md-is-locked-open="$mdMedia(\'gt-sm\')"> \
                <!--  speakers Container  --> \
                <md-list> \
                  <md-title ng-click="sb.showSpeakers = !sb.showSpeakers">Speakers</md-title> \
                  <!--  speaker Container  --> \
                  <md-list-item ng-repeat="speaker in sb.speakerModel.speakers"> \
                    <md-button ng-click="sb.speakerModel.selectSpeaker(speaker)" \
                               ng-class="{\'selected\' : speaker.uid === sb.speakerModel.selectedSpeaker.uid }" \
                               ng-show="sb.showSpeakers"> \
                        {{speaker.player_name}} \
                    </md-button> \
                  </md-list-item> \
                </md-list> \
            </md-sidenav>',
            controller: function() {
                var vm = this;
                vm.showSpeakers = true;
                vm.speakerModel = speakerModel;
            },
            controllerAs: "sb"
        }
    }])      // end of directive

    app.directive('csCurrentTrackinfo', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            templateUrl: "/src/csCurrentTrackInfo.template.html",
            controller: function() {
                var vm = this;
                vm.speakerModel = speakerModel;
            },
            controllerAs: "cti"
        }
    }]);

    app.directive('csCurrentCoordinatorBanner', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            template: '<h2 class="md-title">{{ cci.speakerModel.selectedSpeaker.player_name}} \
                  <i class="material-icons" ng-show="cci.speakerModel.selectedSpeaker.is_playing_tv" aria-label="is playing tv">tv</i> \
                  <i class="material-icons" ng-show="cci.speakerModel.selectedSpeaker.is_playing_line_in">input</i> \
                  <i class="material-icons" ng-show="cci.speakerModel.selectedSpeaker.is_playing_radio">radio</i> \
                  <i class="material-icons" ng-show="cci.speakerModel.selectedSpeaker.is_playing_music">library_music</i> \
                  </h2>',
            controller: function(){
                var vm = this;
                vm.speakerModel = speakerModel;
            },
            controllerAs: 'cci'
        }
    }]);

    app.directive('csCompoundVolume', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            template: '<div layout> \
                        <div flex="10" layout layout-align="center center"> \
                          <span class="md-body-1">Volume ({{ cv.speakerModel.selectedSpeaker.volume }})</span> \
                        </div> \
                        <md-slider class="md-primary" flex md-discrete ng-model="cv.speakerModel.selectedSpeaker.volume" \
                                   step="1" min="0" max="100" aria-label="Volume" \
                                   ng-change="cv.speakerModel.changedNRW(\'volume\')"></md-slider> \
                        <md-switch ng-click="cv.speakerModel.toggleBRW(cv.speakerModel.selectedSpeaker, \'mute\')" \
                                   ng-model="cv.speakerModel.selectedSpeaker.mute" aria-label="Mute">Mute</md-switch> \
                      </div>',
            controller: function() {
                var vm = this;
                vm.speakerModel = speakerModel;
            },
            controllerAs: 'cv'
        }
    }]);

    app.directive('csMediaControls', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            templateUrl: "/src/csMediaControls.template.html",
            controller: function(){
                var vm = this;
                vm.speakerModel = speakerModel;
            },
            controllerAs: "mcCtl"
        }
    }]);

    app.directive('csDebugInfo', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            template: '<div> \
                        uid: {{ di.speakerModel.selectedSpeaker.uid }}  ip: {{di.speakerModel.selectedSpeaker.ip_address }}  play mode: {{ di.speakerModel.selectedSpeaker.play_mode }} \
                        album/artist display: {{ di.speakerModel.selectedSpeaker.album_artist_display_option }} \
                        </div> \
                        <pre>{{ di.speakerModel.selectedSpeaker | json }}</pre>',
            controller: function(){
                var vm = this;
                vm.speakerModel = speakerModel;
            },
            controllerAs: "di"
        }
    }]);

    app.directive('csQueueControl', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            templateUrl: "/src/csQueueControl.template.html",
            controller: function($mdToast){
                var vm = this;
                vm.showSave = false;
                vm.speakerModel = speakerModel;
                vm.searchText = '';

                vm.toggleShowSave = function() {
                    //toggle the vm.showSave setting to hide/show the save q ui
                    console.log('vm.toggleShowSave')
                    vm.showSave = !vm.showSave;
                }

                vm.saveQueueAs = function() {
                    //save queue as
                    vm.searchText = vm.searchText.trim()
                    if (!vm.searchText) {
                        console.log('empty .searchText >< denied');
                        return false;
                    };
                    console.log('.saveQueueAs-> '+vm.searchText);
                    // todo - wire in actual call to save as a sonos queue
                    vm.speakerModel.queueSaveAs(vm.searchText)
                    vm.showSave = false;
                    vm.displayToast('success', 'Queue Saved as: '+vm.searchText);
                };

                vm.queueClear = function() {
                    // clear the queue
                    console.log('queueClear');
                    vm.speakerModel.queueClear();
                    vm.displayToast('success', 'Queue Cleared');
                };

                vm.queueRemove = function(qitem) {
                    // remove selected from queue
                    var fields = qitem.item_id.split('/');
                    var ndx = fields[1];
                    console.log('queueRemove: ' + JSON.stringify(qitem));
                    console.log('queueRemove: ' + ndx);
                    vm.speakerModel.queueRemove(ndx);
                    vm.displayToast('success', 'Removed: ' + qitem.title);
                };

                vm.queuePlay = function(qitem) {
                    var fields = qitem.item_id.split('/');
                    var ndx = fields[1];
                    console.log('queuePlay: ' + JSON.stringify(qitem));
                    console.log('queuePlay: ' + ndx);
                    vm.speakerModel.queuePlay(ndx);
                    vm.displayToast('success', 'Playing: ' + qitem.title);
                };

                vm.displayToast = function(type, msg) {
                    $mdToast.show({
                        template: '<md-toast class="md-toast ' + type +'">' + msg + '</md-toast>',
                        hideDelay: 3000,
                        position: 'bottom right'
                    });
                };
            },
            controllerAs: "qCtrl"
        }
    }]);

    app.directive('csSoundControls', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            templateUrl: "/src/csSoundControls.template.html",
            controller: function(){
                var vm = this;
                vm.speakerModel = speakerModel;
            },
            controllerAs: "sndCtrl"
        }
    }]);

    app.directive('csSpeakerTabs', function() {
        return {
            restrict: "E",
            templateUrl: "/src/csSpeakerTabs.template.html",
            controller: function(){
                var vm = this;
                vm.selectedSpeakerTab = 0;

                vm.onTabSelected = function(tab){
                    console.log('onTabSelected: '+JSON.stringify(tab));
                };

            },
            controllerAs: "stCtrl"
        }
    });

    app.directive('csPlayMode', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            templateUrl: "/src/csPlayMode.template.html",
            controller: function(){
                var vm = this;
                // vm.modes = ['NORMAL', 'LOOP', 'SHUFFFLE', 'LOOP_SHUFFLE']
                vm.speakerModel = speakerModel;

                // vm.pmChange = function() {
                //     var ndx = 0;
                //     if (vm.speakerModel.selectedSpeaker.loop){ ndx = ndx + 1; };
                //     if (vm.speakerModel.selectedSpeaker.shuffle){ ndx = ndx + 2; };
                //     console.log('pmChange: Loop->'+vm.speakerModel.selectedSpeaker.loop +' shuffle->'+vm.speakerModel.selectedSpeaker.shuffle);
                //     console.log('PlayMode: '+ vm.modes[ndx]);
                // }
            },
            controllerAs: "pmCtrl"
        }
    }]);

    app.directive('csBrowseControl', ['speakerModel', function(speakerModel) {
        return {
            restrict: "E",
            templateUrl: "/src/csBrowseControl.template.html",
            controller: function($mdToast){
                var vm = this;
                vm.speakerModel = speakerModel;
                // vm.mdToast = $mdToast;

                vm.browseIcon = function(rtype) {
                    var icon = '';
                    if (rtype === 'Album'){ return 'album'};
                    if (rtype === 'Artist'){ return 'person'};
                    if (rtype === 'SameArtist'){ return 'view_list'};
                    if (rtype === 'Track'){ return 'audiotrack'};
                    if (rtype === 'AlbumList') { return 'tonality' };
                    if (rtype === 'PlayList') { return 'format_align_justify' };
                    if (rtype === 'Genre') { return 'group_work' };
                    return 'build';
                };

                vm.playNow = function(rslt){
                    vm.speakerModel.playNow(rslt);
                    vm.displayToast('success', 'Playing Now');
                };

                vm.queueAdd = function(rslt) {
                    // I exist because displaying Toasts is UI and doesn't
                    // belong in a service like speakerModel
                    vm.speakerModel.queueAdd(rslt);
                    vm.displayToast('success', 'Added to Queue');
                };

                vm.displayToast = function(type, msg) {
                    $mdToast.show({
                        template: '<md-toast class="md-toast ' + type +'">' + msg + '</md-toast>',
                        hideDelay: 3000,
                        position: 'bottom right'
                    });
                };
            },
            controllerAs: "brCtrl"
        }
    }]);

})()
